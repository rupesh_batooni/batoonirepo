-- Drop table

-- DROP TABLE public.campaign_video_details;

CREATE TABLE public.campaign_video_details (
	id BIGINT GENERATED ALWAYS AS IDENTITY,
	campaign_id BIGINT NOT NULL,
	file_name varchar NOT NULL,
	file_size float8 NOT NULL DEFAULT 0.0,
	created_at timestamp DEFAULT NOW(), -- time at which this record was created in database
	updated_at timestamp DEFAULT NOW(),
	CONSTRAINT campaign_video_details_pkey PRIMARY KEY (id)
);
