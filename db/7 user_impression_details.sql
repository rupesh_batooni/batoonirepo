-- Drop table

-- DROP TABLE public.user_impression_details;

CREATE TABLE public.user_impression_details (
	id BIGINT GENERATED ALWAYS AS IDENTITY,
	user_id BIGINT NOT NULL,
	gratification_type_id int4 NULL, -- code of the gratification type defined in lookup table
	viewed_at timestamp NULL, -- time at which the ad was viewed (impression timestamp)
	campaign_id BIGINT NOT NULL,
	video_id BIGINT NULL,
	"location" varchar NULL,
	gratification_value float8 NULL DEFAULT 0.0, -- gratification amount credited for the impression
	ledger_id BIGINT NULL, -- id of the ledger record which has this record's amount
	mobile_impression_id BIGINT NULL, -- id of the impression record saved locally in the mobile app's database during no network connectivity
	created_at timestamp DEFAULT NOW(), -- time at which this record was created in database
	is_current bool NOT NULL DEFAULT true, -- true = included in accounting, false = not included in accounting
	CONSTRAINT user_impression_details_pkey PRIMARY KEY (id),
	CONSTRAINT fk_uid_ledger_id_uld_id FOREIGN KEY (ledger_id) REFERENCES user_ledger_details(id)
);
CREATE INDEX index_user_impression_details_on_user_id ON public.user_impression_details USING btree (user_id);
CREATE INDEX index_user_impression_details_on_campaign_id ON public.user_impression_details USING btree (campaign_id);
CREATE INDEX index_user_impression_details_on_ledger_id ON public.user_impression_details USING btree (ledger_id);