-- Drop table

-- DROP TABLE public.batooni_circlez;

CREATE TABLE public.circlez (
	id BIGINT GENERATED ALWAYS AS IDENTITY,
	circle_name varchar NOT NULL,
	active bool NOT NULL DEFAULT false,
	created_at timestamp default NOW(),
	updated_at timestamp default NOW(),
	CONSTRAINT batooni_circlez_pkey PRIMARY KEY (id)
);
