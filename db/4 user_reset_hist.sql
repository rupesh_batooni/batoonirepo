-- Drop table
-- DROP TABLE public.user_reset_hist;

CREATE TABLE public.user_reset_hist (
	id BIGINT GENERATED ALWAYS AS IDENTITY,
	user_id BIGINT NOT NULL,
	reset_type varchar NULL,
	created_at timestamp default NOW(),
	CONSTRAINT user_reset_hist_pkey PRIMARY KEY (id)
);

--drop INDEX index_user_reset_hist_on_user_id
CREATE INDEX index_user_reset_hist_on_user_id ON public.user_reset_hist USING btree (user_id);