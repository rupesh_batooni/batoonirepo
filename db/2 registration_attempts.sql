-- Drop table
-- DROP TABLE public.registration_attempts;

CREATE TABLE public.reg_attempts (
	id BIGINT GENERATED ALWAYS AS IDENTITY,
	phone_number varchar NOT NULL,
	batooni_circle_id varchar NULL,
	ad_id varchar NOT NULL,
	device_id varchar NULL,
	instance_id varchar NOT NULL,
	otp int4 NULL,
	otp_matched bool NULL DEFAULT false,
	created_at timestamp default NOW(),
	updated_at timestamp default NOW(),
	CONSTRAINT registration_attempts_pkey PRIMARY KEY (id)
);


