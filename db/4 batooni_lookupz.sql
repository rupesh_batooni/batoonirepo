-- Drop table

-- DROP TABLE public.batooni_lookupz;

CREATE TABLE public.batooni_lookupz (
	id BIGINT GENERATED ALWAYS AS IDENTITY,
	table_name varchar NOT NULL,
	code varchar NOT NULL,	
	description varchar NOT NULL,
	abbreviation varchar NULL,
	status bool NULL,
	created_at timestamp DEFAULT NOW(),
	updated_at timestamp DEFAULT NOW(),
	CONSTRAINT batooni_lookupz_pkey PRIMARY KEY (id)
);

--drop INDEX index_batooni_lookupz_on_table_name
CREATE INDEX index_batooni_lookupz_on_table_name ON public.batooni_lookupz USING btree (table_name);