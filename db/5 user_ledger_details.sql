-- Drop table
-- DROP TABLE public.user_ledger_details CASCADE;  -- CASCADE is used to remove the foriegn key constraint in impressions table

CREATE TABLE public.user_ledger_details (
	id BIGINT GENERATED ALWAYS AS IDENTITY,
	txn_mode varchar NULL, -- Use abbr Code - see lookups table
	txn_direction varchar NULL, -- CR / DR
	txn_amount float8 NULL,
	user_id BIGINT NOT NULL, -- user id of the user who got the bonus
	recharge_id BIGINT NULL, -- recharge id of the recharge transaction record from the recharges table
	created_at timestamp DEFAULT NOW(), -- only 'created at' timestamp - ledger records must not have any updates so no 'updated at' timestamp
	is_current bool NOT NULL DEFAULT true, -- true = included in accounting, false = not included in accounting
	CONSTRAINT user_ledger_details_pkey PRIMARY KEY (id)
);

CREATE INDEX index_user_ledger_details_on_user_id ON public.user_ledger_details USING btree (user_id);
CREATE INDEX index_user_ledger_details_on_recharge_id ON public.user_ledger_details USING btree (recharge_id);