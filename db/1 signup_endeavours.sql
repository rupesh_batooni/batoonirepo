-- Drop table
-- DROP TABLE public.signup_endeavours;

CREATE TABLE public.signup_endeavours (
	id BIGINT GENERATED ALWAYS AS IDENTITY,
	phone_number varchar NOT NULL,
	operator_circle varchar NULL,
	ad_id varchar NOT NULL,  -- Mandatory - cannot accept a request without a valid Ad-Id
	device_id varchar NULL,  --  Its not unique and can also be null - all depends on the Android Device Manufacturer
	instance_id varchar NOT NULL, -- Mandatory - cannot accept a request without a valid Instance-Id
	otp int4 NULL,
	otp_matched bool NULL DEFAULT false,
	created_at timestamp default NOW(),
	updated_at timestamp default NOW(),
	CONSTRAINT registration_attempts_pkey PRIMARY KEY (id)
);


