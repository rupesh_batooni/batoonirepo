-- Might need to Create UUID OSSP Extension
-- Refer the uuid-ossp sql

-- DROP TABLE public.userz;
CREATE TABLE public.userz (
	id BIGINT GENERATED ALWAYS AS IDENTITY,
	user_id UUID NOT NU LL DEFAULT uuid_generate_v4(), -- for future use
	passcode varchar NULL, -- for future use, store only hashed value
	phone_number varchar NOT NULL,
	dob date NOT NULL,
	fname varchar NULL,
	lname varchar NULL,
	gender int4 NULL, -- 1 = Male, 0 = Female
	email varchar NULL,
	pin_code varchar NULL,
	user_token varchar NULL, -- security and authorization 
	created_at timestamp DEFAULT NOW(),
	updated_at timestamp DEFAULT NOW(),
	-- last_active_at timestamp NULL, -- TRANSACTIONAL FIELD: update frequency = VERY HIGH |  use a seperate table which will update via a batch job that polls impressions table
	role_id int4 NOT NULL DEFAULT 33, -- 33  = App User
	referral_code varchar NULL,
	app_version varchar NULL, -- version of the android app used by the user
	first_recharge bool NULL DEFAULT false,
	auto_recharge bool NULL DEFAULT false,
	sign_up_notification bool NULL DEFAULT true,
	status int4 NULL DEFAULT 1, -- 1 = Active, 2 = Blocked, 3 = Suspended, 4 = Marked For Deletion
	profession varchar NULL,
	interests varchar NULL, -- Save as JSON
	signup_type int4 DEFAULT 1, -- 1 = Manual Sign Up in Batooni , 2 = oAuth Sign Up like FB/GOogle/TrueCaller etc
	mobile_operator_id int4 NULL,
	oAuth_id varchar NULL,
	circle_id int4 NULL,
	city_id int4 NULL,
	oAuth_token varchar NULL, -- token form oAuth provider
	wallet_id varchar NULL,
	device_id varchar NULL,
	device_name varchar NULL,
	screen_resolution varchar NULL,
	instance_id varchar NOT NULL, -- Instance ID of the Android Application
	CONSTRAINT userz_pkey PRIMARY KEY (id)
);	

--drop INDEX index_userz_on_phone_number
CREATE UNIQUE INDEX index_userz_on_phone_number ON public.userz USING btree (phone_number);



-- ALTER TABLE userz ALTER COLUMN created_at SET DEFAULT now();
-- ALTER TABLE userz ALTER COLUMN updated_at SET DEFAULT now();
