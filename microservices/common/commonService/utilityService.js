const crypto = require("crypto");
const appSecret = require("../../config/secrets.json");
var keyInfoModel = require("../models/keyInfo");

const ENCRYPTION_KEY = appSecret.development.decryption_key; // Must be 256 bits (32 characters)
const ivKey = appSecret.development.decryption_iv;
//var Buffer;
class utilityService {

    constructor() {}

    encrypt(text) {
        let cipher = crypto.createCipheriv("aes-256-cbc", Buffer.from(ENCRYPTION_KEY), Buffer.from(ivKey));
        let ciphertext = cipher.update(Buffer.from(text));
        ciphertext = Buffer.concat([ciphertext, cipher.final()]);

        let encryptText = ciphertext.toString("base64");
        return encryptText;
    }

    decrypt(encryptedText) {
        var buf = new Buffer(encryptedText, "base64");
        var decipher = crypto.createDecipheriv("aes-256-cbc", ENCRYPTION_KEY, ivKey);
        decipher.setAutoPadding(1);

        var dec = decipher.update(buf, "base64");
        dec += decipher.final();
        if (dec.indexOf("_") > 0) {
            return dec.split("_")[1];
        } else
            return dec;
    }

    stringToHash(value){
        return crypto.createHash("sha1").update(value).digest("hex");
    }

    generateUserToken(keyInfo){
        let userKeyInfo = new keyInfoModel();
        userKeyInfo = keyInfo;
        let PhNo = userKeyInfo.phoneNumber;
        let instanceId = userKeyInfo.instanceId;
        let deviceId = userKeyInfo.deviceId;
        let androidId = userKeyInfo.androidId;
        
        return this.stringToHash(PhNo) + "." + this.stringToHash(instanceId) + "." + this.stringToHash(deviceId) + "." + this.stringToHash(androidId);
    }

    splitUserTokenIntoKeyHash(userToken){
        let arrTokenItems = userToken.split(".");
        
        let userKeyInfo = new keyInfoModel();

        userKeyInfo.PhNo = arrTokenItems[0];
        userKeyInfo.instanceId = arrTokenItems[1];
        userKeyInfo.deviceId = arrTokenItems[2];
        userKeyInfo.androidId = arrTokenItems[3];
        
        return userKeyInfo;
    }

    Stringformat(fmt, ...args){
        if (!fmt.match(/^(?:(?:(?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{[0-9]+\}))+$/)) {
            throw new Error("invalid format string.");
        }
        return fmt.replace(/((?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{([0-9]+)\})/g, (m, str, index) => {
            if (str) {
                return str.replace(/(?:{{)|(?:}})/g, m => m[0]);
            } else {
                if (index >= args.length) {
                    throw new Error("argument index is out of range in format");
                }
                return args[index];
            }
        });
    }

}

module.exports = new utilityService();

