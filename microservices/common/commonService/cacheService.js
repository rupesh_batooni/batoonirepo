var masterDb = require("./../../ruleEngine/dataAccess/masterDataDb");
var Enumerable = require("linq");
// var chacheServer URL
// var requestCache server
class globalCache{
    

    constructor(params) {
    this.cityMaster;
    this.stateMaster;
    this.circleMater;
    this.lookupMater;
    this.loadMaster();
    }

    async loadMaster(){
        
        let _circleMater = await masterDb.getCircleMaster();
        if(_circleMater.status) this.circleMater = _circleMater.data;

        let _cityMaster = await masterDb.getCityMaster();
        if(_cityMaster.status) this.cityMaster = _cityMaster.data;
        
        let _stateMaster = await masterDb.getStateMaster();
        if(_stateMaster.status) this.stateMaster = _stateMaster.data;
        
        let _lookupMaster = await masterDb.getLookupMaster();
        if(_lookupMaster.status) this.lookupMater = _lookupMaster.data;
    }

    getCity(cityName){
        return Enumerable.from(this.cityMaster).where((city)=>{ 
            return city.name.toUpperCase() == cityName.toUpperCase();
        }).firstOrDefault();
    }
}

module.exports = new globalCache();