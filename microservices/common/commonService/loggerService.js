const uuidv4 = require("uuid/v4");
const appSvc = require("./appService");
const fs = require("fs");
const moment = require("moment");

//this is an on request logging
// module.exports = function (req, res, next) {
//   console.log("LOGGED on request");
//   next();
// }
var currntTime =  moment().format("LLLL");

var _logData = {
  logId: String, //auto generated uuid
  logDate: String, // now date time stamp
  loggedBy: String, // microservice name
  contextInfo: String, //class name_method name
  isSuccess: String, //error, warning, success
  message: String,
  reqBody: String
};

class logger {
  
  constructor() {}

  logOnRequest(req, res, next) {
    console.log("LOGGED request at ",currntTime);
    next();
  }

  logToFile(contextInfo, isSuccess, message, sendEmail = false,reqBody = {}) {
    _logData.logId = uuidv4();
    _logData.logDate = new Date();
    _logData.loggedBy = appSvc.appTitle;

    _logData.contextInfo = contextInfo;
    _logData.isSuccess = isSuccess;
    _logData.message = message;
    _logData.reqBody = reqBody;

    let logString = JSON.stringify(_logData);

    fs.appendFile("log_" + appSvc.appTitle + ".txt", logString, "utf8",
      // callback function
      function (err) {
        if (err) console.log(err);
        // if no error
        console.log("Data is appended to file successfully.");
      });

    if (sendEmail) {
      sendEmail();
    }

    return {LogId: _logData.logId};
  }

  sendEmail() {

  }

  catchAllError(err, req, res, next) {
    let _logId = this.logToFile("appjs-> catchAllError",false,err.message, req.body);
    res.status(404);
    res.render({statusCode : 404, status : false, message : err.message, data : {logId:_logId}, statusType:"error"});
  }

}

module.exports = new logger();