//var payload = require("../../models/payload");
var appConfig = require("./../../config/appConfig.json");
var payToAll = require("./../../config/pay2all.json");
var secret = require("./../../config/secrets.json");
var appMsg = require("./../../config/appMessage.en.json");
var logSvc = require("./loggerService");


module.exports.isRegistrationOff = appConfig.isRegistrationOff; //TODO: read from system setting
module.exports.payToall = payToAll;
module.exports.secret = secret;
module.exports.appMsg = appMsg.en;
module.exports.envPayToAll = appConfig.envPayToall;
module.exports.isProduction = appConfig.environment != "dev";
module.exports.pgConn = appConfig.pgConn;

module.exports.appTitle = appConfig.appTitle;

module.exports.maxOTPResend = appConfig.maxOTPResend;
module.exports.maxAndroidIdReg = appConfig.maxAndroidIdReg;
module.exports.otpDefault = appConfig.otpDefault;
module.exports.smsApi = appConfig.sms_api;
module.exports.maxSignup = appConfig.maxSignup;
module.exports.maxSignupDaysLimit = appConfig.maxSignupDaysLimit;
module.exports.userLastSignupDaysLmit = appConfig.userLastSignupDaysLmit;
module.exports.maxUserRegistrationLimit = appConfig.maxUserRegistrationLimit;

module.exports.handleError = function (error, req, res, contextInfo) {
    let _logId = logSvc.logToFile(contextInfo, false, error.stack, false, req.body);
    return res.json(this.returnFailed(error.message, _logId));
};

module.exports.returnSuccess = function (message = "", data = {}) {
    return {
        statusCode: 200,
        status: true,
        message: message,
        data: data,
        statusType: "success"
    };
};
module.exports.returnFailed = function (message = "", data = {}) {
    return {
        statusCode: 404,
        status: false,
        message: message,
        data: data,
        statusType: "error"
    };
};