var keyInfo = require("./keyInfo");
class user {

	constructor() {

		this.method = "";
		this.firstName = "";
		this.lastName = "";
		this.phoneNumber = "";
		this.serviceType = "";
		this.providerName = "";
		this.email = "";
		this.referralCode = "";
		this.mobileTime = "";
		this.gender = "";
		this.city = "";
		this.pinCode = "";
		this.state = "";
		this.country = "";
		this.dob = "";
		this.deviceId = "";
		this.userName = "";
		this.deviceName = "";
		this.passcode = "";
		this.screenResolution = "";

		//more output
		this.userToken = "";
		this.totalCoupon = "";
		this.totalPoint = "";
		this.regTime = "";

		//other fields for admin use
		this.circleId = "";
		this.mobileOperatorId = "";
		this.roleId = "";
		this.appVersion = "";
		this.profession = "";
		this.interests = "";
		this.signupType = "";

		//more security fields
		this.oauthId = "";
		this.oauthToken = "";
		this.walletId = "";
		this.userToken = "";
		this.instanceId = "";
		this.keyInfo = new keyInfo();
	}

	//as per sign_up api input

}

module.exports = user;