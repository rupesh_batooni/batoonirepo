
exports.statusType = {
    success : "success",
    error : "error",
    warning : "warning"
};
exports.response = {
    data  : undefined,
    status:true,
    message:"",
    statusCode:200,
    statusType:"" //warning,success,error
};