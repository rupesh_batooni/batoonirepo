
class keyInfo {
  
    constructor() {
        this.phoneNumber = "";
        this.deviceId = "";
        this.operatorCircle = "";
        this.androidId = "";
        this.instanceId = "";
        this.otp = "";
        this.packageId = "";
        this.userToken = "";
        this.deviceName = "";
        this.screenResolution = "";
    }
    
}

module.exports = keyInfo;