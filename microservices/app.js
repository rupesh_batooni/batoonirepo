/* eslint-disable no-undef */
 "use strict ";

var SwaggerExpress = require( "swagger-express-mw");
var SwaggerUi = require( "swagger-tools/middleware/swagger-ui");
var app = require( "express")();
var bodyParser = require( "body-parser");
var interceptor = require( "./api/apiService/requestValidationService");
var  logger = require( "./common/commonService/loggerService");
const helmet = require("helmet");
var moment = require("moment");

var session = require("cookie-session");
var expiryDate = moment().add(1,"h").format(); // 1 hour

module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function (err, swaggerExpress) {
  if (err) { throw err; }

  app.use(bodyParser.json());

  // security implementation
  //TODO: change domain name
  app.use(helmet());
  app.use(session({
    name: "session",
    keys: ["key1", "key2"],
    cookie: {
      secure: true,
      httpOnly: true,
      domain: "batooni.com",
      path: "foo/bar",
      expires: expiryDate
    }
  }));

  ///////////////////////////////

  //add swagger-ui
  app.use(SwaggerUi(swaggerExpress.runner.swagger));

  //injecting logger in middleware
  app.use(logger.logOnRequest);
  app.use(interceptor.validate);

 //Implement the “catch-all” errorHandler after all middleware
 app.use(logger.catchAllError);

 // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 3000;
  app.listen(port, ()=>{
    console.log("listening on port:", port);
    console.log("modment: ",moment().format("LLLL"));
  });

});
