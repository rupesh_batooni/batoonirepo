// const request = require("supertest");
// const app =  require("../../../app");

// //==================== user API test ====================

// describe("POST /checkUserPhoneNumber", function () {
//   let data = {
//     "reqInfo":{"phoneNumber":"XKbktZWVj7D1Dq7PqxhYVE40ykbvPOwHVL8n1\/92AII=\n","deviceId":"868139029778369","androidId":"klasfklas90938929ksdf","instanceId":"klasjd9asffas89f7as","pacakgeId":"klasjd9asffas89f7as"}
//     };

//   it("POST /checkUserPhoneNumber", function (done) {
//       request(app)
//       .post('/api/checkUserPhoneNumber2', {
//                 json: true,
//                 body: data
//               })
//           .set("Accept", "application/json")
//           .expect("Content-Type", /json/)
//           //.expect(200)
//           // .expect(function(res) {
//           //   res.body.statusCode = 200;
//           //   res.body.status = true;
//           //   res.body.message = "OTP sent to provider.";
//           //   res.body.data = {
//           //       "isOtpSent": true
//           //   };
//           //   res.body.statusType = "success";
//           // })
//           .expect(200,{
//             statusCode: 200,
//             status: true,
//             message: "OTP sent to provider.",
//             data: {
//                 "isOtpSent": true
//             },
//             statusType: "success"
//         }, done)
//         .end(function(err, res) {
//           console.log("inside from test user regisgration");
//           if (err) throw err;
//         });

//         done();
//   });
// });



var should = require('should');
var request = require('supertest');
var server = require('../../../app');

describe('controllers', function () {

  it('POST /checkUserPhoneNumber', function (done) {
    let data = {
      "reqInfo": {
        "phoneNumber": "XKbktZWVj7D1Dq7PqxhYVE40ykbvPOwHVL8n1\/92AII=\n",
        "deviceId": "868139029778369",
        "androidId": "klasfklas90938929ksdf",
        "instanceId": "klasjd9asffas89f7as",
        "pacakgeId": "klasjd9asffas89f7as"
      }
    };

    request(server)
      .post('/api/checkUserPhoneNumber', {
        json: true,
        body: data
      })
      .set('Accept', 'application/json')
      .send(data)
      .expect('Content-Type', /json/)
      .expect(404)
      // .expect(function(res) {
      //               res.body.statusCode = 200;
      //               res.body.status = true;
      //               res.body.message = "OTP sent to provider.";
      //               res.body.data = {
      //                   "isOtpSent": true
      //               };
      //               res.body.statusType = "success";
      //             })
      // .expect(200,function (response) {
      //   debugger;
      //   console.log("inside from test user regisgration");
      //   response.should.eql({
      //     statusCode: 200,
      //     status: true,
      //     message: "OTP sent to provider.555",
      //     data: {
      //       "isOtpSent": true
      //     },
      //     statusType: ""
      //   });
      // })
      
      // .end(done);
     
      .end(function (err, res) {
        console.log("inside from test user regisgration", err);
        should.not.exist(err);

        res.body.should.eql({
          statusCode: 200,
          status: true,
          message: "OTP sent to provider.555",
          data: {
            "isOtpSent": true
          },
          statusType: "success"
        });
      });
    done();
  });

});