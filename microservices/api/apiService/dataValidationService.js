//var utilityService = require("../commonService/utilityService");
var appSvc = require("../../common/commonService/appService");

class dataValidation {
    constructor() {}
    validatePhoneNumber(phoneNumber) {
        var regex = "^[2-9]{2}[0-9]{8}$";
        let isValid = phoneNumber.match(regex) != undefined;
        //appSvc.returnFailed().message = "Invalid Phone Number";
        return isValid ? appSvc.returnSuccess(): appSvc.returnFailed("Invalid Phone Number");
    }
}


module.exports = new dataValidation();
