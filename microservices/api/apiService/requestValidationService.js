var appService = require("../../common/commonService/appService");
var utilitySvc = require("../../common/commonService/utilityService");
var dataValidationSvc = require("./dataValidationService");
var logSvc = require("../../common/commonService/loggerService");
//1. user agent validation
//2. Max Phone number registration validation based upon Phone-android_id count
//3. 

class requestValidation {

    async validate(req, res, next) {
        var userAgent = req.headers["HTTP_USER_AGENT"] || req.headers["user-agent"];
        if (appService.isProduction && userAgent.os_name != "Android") {
            return res.json(appService.returnFailed("not_authorized"));
        }
        let regDetail = req.body.reqInfo;
        var decryptedPhone = utilitySvc.decrypt(regDetail.phoneNumber);
        req.body.reqInfo.phoneNumber = decryptedPhone;

        let resultValidPhoneNumber = dataValidationSvc.validatePhoneNumber(regDetail.phoneNumber);
        if (!resultValidPhoneNumber.status) {
            return res.json(resultValidPhoneNumber);
        }

        //console.log("encrytion of ph number: 9754424248 is ",utilitySvc.encrypt("9754424248"));

        process.on("unhandledRejection", (err) => {
            console.log(err);
            logSvc.logToFile("unhandledRejection",false,err);
          });

        next();
    }
}
module.exports = new requestValidation();