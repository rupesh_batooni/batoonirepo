"use strict";
//#region [Imports] 
var user = require("../../../common/models/user");
var ruleSvc = require("../../../ruleEngine/registrationRule");
var appService = require("../../../common/commonService/appService");
var regDetail = require("../../../common/models/keyInfo");
//var loggerSvc = require("./../../common/service/commonService/loggerService");
//#endregion

class userRegistration {
 
    async checkUserPhoneNumber(req, res) {
        try {
            let _regDetail = new regDetail();
            _regDetail = req.body.reqInfo;

            var result = await ruleSvc.checkUserPhoneNumber(_regDetail);
            return res.json(result);

        } catch (error) { //catching any rejection from promise
            return appService.handleError(error, req, res, "checkUserPhoneNumber");
        }
    }

    //1. resend otp upto x limit
    async resendOTP(req, res) {
        var keyInfo = req.body.reqInfo;
        try {
            let _resendOTPResult = await ruleSvc.resendOtp(keyInfo);
            return  res.json(_resendOTPResult);
        } catch (error) {
            return appService.handleError(error, req, res, "resendOtp");
        }
    }

    //1. update otp matched
    //2. send user status for existing/new
    async confirmOTP(req, res) {
        try {
            let _keyInfo = req.body.reqInfo;

            let _resultConfirmOtp = await ruleSvc.confirmOtp(req.body.otp, _keyInfo);

            if (_resultConfirmOtp.status) {
                _resultConfirmOtp.message = "OTP confirmed!";
            }

            return res.json(_resultConfirmOtp);
        } catch (error) {
            
            return res.json(error);
        }
    }

    //1. send user dob status for matched/Not Matched
    //2. send user status for existing/new
    async confirmDOB(req, res) {
        try {
            let _confirmDob = await ruleSvc.confirmDob(req.body.reqInfo,req.body.dob);

            return res.json(_confirmDob);
        } catch (error) {
            return appService.handleError(error, req, res, "confirmDOB");
        }
    }

    async restoreAccount(req, res) {
        try {

            return res.json(appService.returnSuccess());
        } catch (error) {
             return appService.handleError(error, req, res, "restoreAccount");
        }
    }



    async signUp(req, res) {
        var _user = new user();
        _user = req.body;
        try {
            var _signUpUser = await ruleSvc.signUpUser(_user);
            return res.json(_signUpUser);

        } catch (error) { 
            return appService.handleError(error, req, res, "signUp");
        }
    }
}

module.exports = new userRegistration();