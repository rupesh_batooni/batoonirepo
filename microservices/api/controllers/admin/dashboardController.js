var pgHelper = require("./../../../common/dataAccess/pgDbHelper.js");
var payload = require("./../../../common/models/payload");

exports.getCircles = function (req, res) {
    pgHelper.pgQuery("SELECT * from batooni_circles").then(function (data) {
            console.log("getcircle called", data);
            return res.json(data);
        })
        .catch(function (err) {
            console.log("error occured in getCircles", err);
            var _res = handleError(err);
            return res.json(_res);
        });
};


exports.getBatooniSpread = function (req, res) {
    pgHelper.pgQuery("SELECT * from vw_batooni_spread").then(function (data) {
            console.log("get batooni_spread called", data);
            return res.json(data);
        })
        .catch(function (err) {
            console.log("error occured in get batooni_spread", err);
            var _res = handleError(err);
            return res.json(_res);
        });
};

exports.getYesterdayTrend = function (req, res) {
    pgHelper.pgQuery("SELECT day_quarter, day_sum	FROM public.'fn_YesterdaySixHourlyIntradayTrend'()").then(function (data) {
            console.log("get YesterdayTrend called", data);
            return res.json(data);
        })
        .catch(function (err) {
            console.log("error occured in getYesterdayTrend", err);
            var _res = handleError(err);
            return res.json(_res);
        });
};

exports.getTodayTrend = function (req, res) {
    pgHelper.pgQuery("SELECT day_quarter, day_sum	FROM public.'fn_TodaySixHourlyIntradayTrend'()").then(function (data) {
            console.log("getTodayTrend called", data);
            return res.json(data);
        })
        .catch(function (err) {
            console.log("error occured in getTodayTrend", err);
            var _res = handleError(err);
            return res.json(_res);
        });
};

exports.getTotalRegSinceLaunch    = function (req, res) {
    pgHelper.pgQuery("SELECT * from  public.'fn_CountRegisteredUsersSinceLaunch'()").then(function (data) {
            console.log("getTotalRegSinceLaunch called", data);
            return res.json(data);
        })
        .catch(function (err) {
            console.log("error occured in getTotalRegSinceLaunch", err);
            var _res = handleError(err);
            return res.json(_res);
        });
};

//SELECT * from public."fn_RegUserGenderWise"()
exports.getRegUserGenderWise    = function (req, res) {
    pgHelper.pgQuery("SELECT * from  public.'fn_RegUserGenderWise'()").then(function (data) {
            console.log("getRegUserGenderWise called", data);
            return res.json(data);
        })
        .catch(function (err) {
            console.log("error occured in getRegUserGenderWise", err);
            var _res = handleError(err);
            return res.json(_res);
        });
};

var handleError = function (err) {
    payload.response.data = undefined;
    payload.response.message = err;
    payload.response.status = payload.statusType.error;
    return payload.response;
};