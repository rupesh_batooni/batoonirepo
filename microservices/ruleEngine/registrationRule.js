/* eslint-disable require-atomic-updates */
//#region [Imports]
var appService = require("../common/commonService/appService");
var ruleSvc = require("./ruleService/ruleService");
var keyInfoModel = require("../common/models/keyInfo");
var logSvc = require("../common/commonService/loggerService");
var signupEndvDb = require("./dataAccess/signupEndeavoursDb");
var userDb = require("./dataAccess/userDb");
var circleDb = require("./dataAccess/circleDb");
var moment = require("moment");
var deviceDtlDb = require("./dataAccess/userDeviceDetailDb");
var globalCache = require("../common/commonService/cacheService");
//#endregion
class registrationRule {

    constructor() {}

    // 1. Validate phone number
    // 2. Get circle from provider
    // 3. Generate otp
    // 4. Limit validation for AndroidId registration
    // 5. Create signupEndeavor record
    // 6. Validate Circle for Batooni open circle
    // 7. Send otp to provider
    async checkUserPhoneNumber(regDetail) {
        let keyInfo = new keyInfoModel();
        keyInfo = regDetail;
        try {
            let regCountResult = await signupEndvDb.androidIdRegCount(keyInfo);
            if (regCountResult.data.count > appService.maxAndroidIdReg) {
                return appService.returnFailed("This device reached maximum registration limit");
            }

            //TODO: hard code circle check
            //var operatorDetail = JSON.parse("{\"status\":0,\"data\":{\"OrderId\":\"A190821175725KJZO\",\"reqid\":\"2111\",\"lookup_number\":\"919754424248\",\"country\":\"India\",\"operator_circle\":\"IDEA,Madhya Pradesh\",\"ported\":false,\"response_time\":\"2019-08-21 17:57:26\",\"Status\":\"COMPLETED\",\"status_code\":\"SDL\",\"desc\":\"DELIVERED\"}}");
            var operatorDetail = await ruleSvc.getOperatorDetail(keyInfo.phoneNumber);

            if (!operatorDetail.data) {
                return appService.returnFailed("Phone Number not valid");
            }

            if (operatorDetail.data) {
                let tempArr;
                if (operatorDetail.data.ported) {
                    tempArr = operatorDetail.data.new_operator_circle.split(",");
                } else {
                    tempArr = operatorDetail.data.operator_circle.split(",");
                }

                keyInfo.operatorCircle = tempArr[tempArr.length - 1].replace(/\s[!@%&"]/, "");

                var circleOpen = await circleDb.isCircleOpened(keyInfo.operatorCircle);

                let newOtp;
                if (circleOpen.status) {
                    newOtp = ruleSvc.generateOtp();
                    keyInfo.otp = newOtp;
                }

                var regAttemptResult = await signupEndvDb.createSignUpEndv(keyInfo);
                if (!regAttemptResult.status) {
                    let _logId = logSvc.logToFile("registrationRule->keyInfo", false, appService.returnFailed());
                    return appService.returnFailed("System failure. Please contact administrator!",_logId);
                }

                if (!circleOpen.status) {
                    let _logId = logSvc.logToFile("registrationRule->keyInfo", false, appService.returnFailed());
                    return appService.returnFailed("Circle " + keyInfo.operatorCircle + " Not Opened",_logId);
                }

                return this._sendOtp(keyInfo);
            }

        } catch (error) {
            return appService.returnFailed(error.message);
        }
    }

    async _sendOtp(keyInfo) {
        let sentOtp = await ruleSvc.otpSendRequestToProvider(keyInfo.otp, keyInfo.phoneNumber);
        var _data;
        var _message;
        if (sentOtp.type == "success") {
           
            // eslint-disable-next-line require-atomic-updates
            _message = "OTP sent to provider.";
            _data = {
                isOtpSent: true
            };
            if (!appService.otpDefault.sendToProvider) {
                _message += " otp: " + sentOtp.generatedOtp;
            }
            return appService.returnSuccess(_message,_data);
        } else {
            _data = {
                isOtpSent: false
            };
            return appService.returnFailed( "OTP sent to provider failed",_data);
        }
    }
    //1.if otp matched: update the same; return status and isNewUser flag
    async confirmOtp(otp, pKeyInfo) {
        let keyInfo = new keyInfoModel();
        keyInfo = pKeyInfo;
        var _data;
        var _message;
        try {
            let otpMatch = await signupEndvDb.updateOtpMatch(otp, keyInfo);
            if (otpMatch.status) {
                //TODO: nKey update
                let userDetail = await userDb.findUser(keyInfo.phoneNumber);
                _data = {
                    isNewUser: userDetail.status,
                    isOTPMatched: true
                };
                return appService.returnSuccess("",_data);
            } else {
                _data = {
                    isOTPMatched: false
                };
                _message = appService.appMsg.errors.otp_not_confirmed;
                return otpMatch(_message);
            }

        } catch (error) {
            return appService.returnFailed(error.message);
        }

    }

    async resendOtp(keyInfo) {
  
        try {

            let otpCount = await signupEndvDb.resendOtpCount(keyInfo);

            if (otpCount.status && otpCount.data.count >= appService.maxOTPResend) {
                return appService.returnFailed(appService.appMsg.errors.OTPResendLimit);
            }


            var newOtp = ruleSvc.generateOtp();
            keyInfo.otp = newOtp;

            var regAttemptResult = await signupEndvDb.createSignUpEndv(keyInfo);
            if (!regAttemptResult.status) {
                let _logId = logSvc.logToFile("registrationRule->keyInfo", false, appService.returnFailed());
                return appService.returnFailed(appService.appMsg.errors.system_failure,_logId);
            }

            var _sendOtpResult = this._sendOtp(keyInfo);
            return _sendOtpResult;

        } catch (error) {
            return appService.returnFailed(error.message);
        }

    }

    //1.if otp matched: update the same; return status and isNewUser flag
    async confirmDob(keyInfo, dobToConfirm) {

        var _dobToConfirm;
        var _data;
        //moment("12-25-1995", "MM-DD-YYYY");
        if (moment(dobToConfirm, "DD-MM-YYYY", true).isValid()) {
            _dobToConfirm = moment(dobToConfirm, "DD-MM-YYYY");
        }

        if (moment(dobToConfirm, "DD/MM/YYYY", true).isValid()) {
            _dobToConfirm = moment(dobToConfirm, "DD/MM/YYYY");
        }

        var userDob;
        try {
            let userDetail = await userDb.findUser(keyInfo.phoneNumber);
            if (userDetail.status) {
                let user = userDetail.data;
                if (user.dob) {
                    userDob = moment(user.dob, "YYYY-MM-DD");
                    let _dayDiff = userDob.diff(_dobToConfirm, "days");
                    if (_dayDiff == 0) {
                        _data = {
                            isNewUser: false,
                            isDOBmatched: true
                        };
                        return appService.returnSuccess("",_data);
                    }

                }

               // appService.returnFailed().message = "DOB mismatch";
                _data = {
                    isNewUser: false,
                    isDOBmatched: false
                };
                return appService.returnFailed("DOB mismatch",_data);
            }

            _data = {
                isNewUser: true,
                isDOBmatched: false
            };
            return appService.returnFailed("User does not exist",_data);

        } catch (error) {
            return appService.returnFailed(error.message);
        }

    }

    // Registrations On/Off: If  Registrations are Off then new users cannot register.
    // Max Users limit: Max users in system limit.
    // max 2 times in 15 days // Max registration attempts: Max registration attempts settings locks out the user for a set number of days.
    // if existing user: update/override user detail; else: create new user
    // create entry into user reset detail

    async signUpUser(signupUser) {
        var phoneNumber = signupUser.reqInfo.phoneNumber;
        try {
            //verify registration is ON/OFF
            if (appService.isRegistrationOff) {
                return appService.returnFailed(appService.appMsg.errors.registration_full);
            }

            let _foundMaxUserLimit = await ruleSvc.foundMaxUserLimit();
            if (_foundMaxUserLimit.status) {
                return appService.returnFailed(appService.appMsg.errors.maxUserLimit);
            }

            //check registration attempt is successful
            let _otpMatched = await signupEndvDb.findLatestOtpMatch(phoneNumber);
            if (!_otpMatched) {
                _otpMatched.message = appService.appMsg.errors.no_registration_attempt;
                return _otpMatched;
            }
            let userDetail = await userDb.findUser(phoneNumber);


            //check for maximum no of sign up with x no of days
            if (userDetail.status) {
                let _foundMaxSignUp = await ruleSvc.foundMaxNoOfSignUP(phoneNumber);

                if (_foundMaxSignUp.status) {
                    return appService.returnFailed(_foundMaxSignUp.message);
                }
            }

            debugger;
            let cityDetail = globalCache.getCity(signupUser.city);
            signupUser.cityId = cityDetail.id;
            signupUser.circleId = 1; //TODO: get from cache
            signupUser.mobileOperatorId = 1; //TODO: get from cache
            signupUser.phoneNumber = phoneNumber;
            signupUser.reqInfo.userToken = signupUser.reqInfo.passcode;

            if (userDetail.status) {

                let userUpdate = await userDb.updateUser(signupUser); // overriding all attribute of user profile
                if (userUpdate.status) {

                    let updateDeviceDetail = await deviceDtlDb.update(signupUser.reqInfo,userUpdate.data[0].id);

                    if (!updateDeviceDetail.status) {
                        logSvc.logToFile("signUpUser", false, updateDeviceDetail.message);
                        return appService.returnFailed("user device detail has some issue.");
                    }
                } else {
                    logSvc.logToFile("signUpUser", false, userUpdate.message);
                    return appService.returnFailed("user update has some issue.");
                }

            } else {
                // case of new user
                let userInsert = await userDb.createUser(signupUser);
                if (userInsert.status) {
                    let deviceDetailInsert = await deviceDtlDb.create(signupUser.reqInfo, userInsert.data[0].id);

                    if (!deviceDetailInsert.status) {
                        return appService.returnFailed("user device detail has some issue.");
                    }
                } else {
                    logSvc.logToFile("signUpUser", false, userInsert.message);
                    return appService.returnFailed( "user create has some issue.");
                }
            }

            //create user reset detail

            //create user_referral record if referral_code present in request and found in userz table

            //credit bonus
            // let _creditBonus = ruleSvc.creditBonus(signupUser, userDetail.status);

            // if (!_creditBonus.status) {
            //     logSvc.logToFile("credit bonus on sign up", false, _creditBonus.message);
            // }

            userDetail = await userDb.findUser(signupUser.phoneNumber);
            return userDetail;

        } catch (error) {
            //appService.returnFailed().message = error.message;
            //return appService.returnFailed();
            throw new Error(error.message);
            //appService.returnFailed();
        }
    }

    //1. Restore case - after x day of registration, user can restore any number of times
    async userRestore(signupUser) {

    }

}

module.exports = new registrationRule();