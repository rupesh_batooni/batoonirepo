//region [Imports]
var pgHelper = require("./pgDbHelper");
var appService = require("./../../common/commonService/appService");
var logSvc = require("./../../common/commonService/loggerService");
var keyInfoModel = require("./../../common/models/keyInfo");
//endregion

class userDeviceDetailDb {

    constructor() {}

    find(phoneNumber) {
        //TODO: all query should be read from json file
        var selectQuery = "select * from public.user_device_details where phone_number = $1;";

        var selectParam = [phoneNumber];
        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(selectQuery, selectParam).then(function (data) {
                    if (data.length == 0) {
                        resolve(appService.returnFailed());
                    } else {
                        resolve(appService.returnSuccess("", data[0]));
                    }
                })
                .catch(function (err) {
                    console.log("error occured in find regitstration attampt", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }

    create(keyInfo, userId) {
        let _keyInfo = new keyInfoModel();
        _keyInfo = keyInfo;

        var insertQuery = "INSERT INTO public.user_device_details (phone_number, user_token, device_id,  device_name, instance_id, android_id, package_id, screen_resolution, user_id) " +
            " VALUES ( $1,$2,$3,$4,$5,$6,$7,$8,$9)  RETURNING id;";
        var insertParam = [_keyInfo.phoneNumber, _keyInfo.userToken, _keyInfo.deviceId, _keyInfo.deviceName, _keyInfo.instanceId, _keyInfo.androidId, _keyInfo.packageId, _keyInfo.screenResolution, userId];

        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(insertQuery, insertParam).then(function (data) {

                    if (data.length == 0) {
                        logSvc.logToFile("create user device detail", false, "user device detail update error");
                        reject(appService.returnFailed("create user device detail error"));
                    } else {
                        resolve(appService.returnSuccess("user device detail created", data));
                    }
                })
                .catch(function (err) {
                    console.log("error occured in user signedup", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }

    update(keyInfo, userId) {
        let _keyInfo = new keyInfoModel();
        _keyInfo = keyInfo;
        var insertQuery = "update public.user_device_details set user_token = $1, device_id = $2,  device_name = $3, instance_id = $4, android_id = $5, package_id = $6, screen_resolution = $7 " +
            " where phone_number = $8 and user_id = $9 RETURNING id;";
        var insertParam = [_keyInfo.userToken, _keyInfo.deviceId, _keyInfo.deviceName, _keyInfo.instanceId, _keyInfo.androidId, _keyInfo.packageId, _keyInfo.screenResolution, _keyInfo.phoneNumber, userId];

        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(insertQuery, insertParam).then(function (data) {

                    if (data.length == 0) {
                        logSvc.logToFile("update user device detail", false, "user device detail update error");
                        reject(appService.returnFailed("user device detail update error"));
                    } else {
                        resolve(appService.returnSuccess());
                    }
                })
                .catch(function (err) {
                    console.log("error occured in user signedup", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }

}

module.exports = new userDeviceDetailDb();