const { Client, Pool } = require("pg");
const env = require("./../../config/appConfig.json");
//var appService = require("../service/commonService/appService");
// var connectPG = function () {
//   return new Promise((resolve, reject) => {
//     var client = new Client({
//       connectionString: env.pgConn,
//     });

//     client.connect()
//       .then(function () {
//         resolve(client);
//       })
//       .catch(function (err) {
//         console.log(err);
//         client.end();
//         reject(err);
//       });
//   });
// };

// exports.pgQuery = function (selectQuery) {
//   return new Promise((resolve, reject) => {
//     connectPG().then(function (client) {
//         client.query(selectQuery, (err, result) => {
//           if (err) {
//             console.log("error occured in query to PG", err);
//             client.end();
//             reject(err);
//           } else {
//             client.end();
//            resolve(result.rows);

//           }
//         });
//       })
//       .catch(function (err) {
//         console.log("error occured in pg connect", err);
//         //client.end();
//         reject(err);
//       });

//   });

// };


// exports.pgCommand = function (sqlStatement, sqlParam) {
//   return new Promise((resolve, reject) => {
//     connectPG().then(function (client) {
//         client.query(sqlStatement, sqlParam, (err, result) => {
//           console.log(err, result);
//           if (err) {
//             console.log("error occured in query to PG", err);
//             client.end();
//             reject(err);
//           } else {
//             client.end();
//            resolve(result.rows);

//           }
//         });
//       })
//       .catch(function (err) {
//         console.log("error occured in pg connect", err);
//         //client.end();
//         reject(err);
//       });

//   });

// };

//var handleError = appService.handleError;

/////////////////////////////using connection pool//////////////////////
const pool = new Pool(env.pgConnPool);

// the pool will emit an error on behalf of any idle clients
// it contains if a backend error or network partition happens
pool.on("error", (err, client) => {
  console.error("Unexpected error on idle client", err);
  process.exit(-1);
});


exports.pgCommand = async function (sqlStatement, sqlParam) {
  const client = await pool.connect();
  try {

    const res = await client.query(sqlStatement, sqlParam);
    return (res.rows);

  } catch (error) {
    throw new Error(error);

  } finally {
    // Make sure to release the client before any error handling,
    // just in case the error handling itself throws an error.
    client.release();
  }
};


//using pool only without explicit client so that pool will pick up idle client and it will avoid client leak
exports.pgQuery = async function (selectQuery, selectParam = []) {
  try {
    const res = await pool.query(selectQuery, selectParam);
    return res.rows;
  } catch (error) {
    console.log("error occured in pg connect", error);
    throw new Error(error);
  }
};

exports.pgTransaction = async function(parentQueryReturnById, parentParams, childQuery, childParams){
  const client = await pool.connect();
  try {
    await client.query("BEGIN");
    const resParent = await client.query(parentQueryReturnById, parentParams);
    console.log(resParent.rows);

    childParams.add(resParent.rows[0].id);
    const resChild = await client.query(childQuery, childParams);
    console.log(resChild.rows);
    await client.query("COMMIT");

    return resChild.rows;

  } catch (error) {
    console.log(error.stack);
    await client.query("ROLLBACK");
    throw new Error(error);

  } finally {
    client.release();
  }
};

// (async () => {
//   // note: we don't try/catch this because if connecting throws an exception
//   // we don't need to dispose of the client (it will be undefined)
//   const client = await pool.connect()
//   try {
//     await client.query('BEGIN')
//     const queryText = 'INSERT INTO users(name) VALUES($1) RETURNING id'
//     const { rows } = await client.query(queryText, ['brianc'])
//     const insertPhotoText = 'INSERT INTO photos(user_id, photo_url) VALUES ($1, $2)'
//     const insertPhotoValues = [res.rows[0].id, 's3.bucket.foo']
//     await client.query(insertPhotoText, insertPhotoValues)
//     await client.query('COMMIT')
//   } catch (e) {
//     await client.query('ROLLBACK')
//     throw e
//   } finally {
//     client.release()
//   }
// })().catch(e => console.error(e.stack))