var pgHelper = require("./pgDbHelper");
var keyInfoModel = require("./../../common/models/keyInfo");
var logSvc = require("./../../common/commonService/loggerService");
var appService = require("./../../common/commonService/appService");

class signupEndeavoursDb {

    findLatestOtpMatch(phoneNumber) {
        //TODO: all query should be read from json file
        var selectQuery = "select * from public.signup_endeavours where phone_number = $1 and otp_matched=true  and created_at = (select max(created_at) from public.signup_endeavours where phone_number = $1);";

        var selectParam = [phoneNumber];
        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(selectQuery, selectParam).then(function (data) {
                    if (data.length == 0) {
                        resolve(appService.returnFailed());
                    } else {
                        resolve(appService.returnSuccess());
                    }
                })
                .catch(function (err) {
                    console.log("error occured in find regitstration attampt", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }

    updateOtpMatch(otp, keyInfo) {
        var query = "UPDATE public.signup_endeavours SET otp_matched = true where phone_number = $1  and otp = $5 and  android_id = $2 and device_id = $3 and instance_id = $4 and created_at = (select max(created_at) from public.signup_endeavours where phone_number = $1)  RETURNING id;";
        var param = [keyInfo.phoneNumber, keyInfo.androidId, keyInfo.deviceId, keyInfo.instanceId, otp];
        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(query, param).then(function (data) {
                    if(data.length == 0) {
                        reject(appService.returnFailed("OTP not matched"));
                    }

                    resolve(appService.returnSuccess());
                })
                .catch(function (err) {
                    let _logId = logSvc.logToFile("registraionRule->confirmOtp", false, err.message);
                    reject(appService.returnFailed("",_logId));
                });
        });
    }

    resendOtpCount(keyInfo) {
        var query = "SELECT count(1)  FROM public.signup_endeavours where to_char(created_at, 'DD/MM/YYYY') = (select to_char(max(created_at), 'DD/MM/YYYY')  from public.signup_endeavours where phone_number = $1) and phone_number = $1;";
        var param = [keyInfo.phoneNumber];
        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(query, param).then(function (data) {
                    resolve(appService.returnSuccess(data[0]));
                })
                .catch(function (err) {
                    let _logId = logSvc.logToFile("signupEndeavoursDb->resendOtpCount", false, err.message);
                    reject(appService.returnFailed(_logId));
                });
        });
    }

    androidIdRegCount(keyInfo) {
        var query = "SELECT count(1)  FROM public.signup_endeavours where otp_matched = true and android_id = $1;";
        var param = [keyInfo.androidId];
        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(query, param).then(function (data) {
                    resolve(appService.returnSuccess("", data[0]));
                })
                .catch(function (err) {
                    let _logId = logSvc.logToFile("signupEndeavoursDb->resendOtpCount", false, err.message);
                    reject(appService.returnFailed(_logId));
                });
        });
    }

    createSignUpEndv(regDetail) {
        let keyInfo = new keyInfoModel();
        keyInfo = regDetail;
        //TODO: ID indentity column get incremented even on failed transaction
        var insertQuery = "INSERT INTO public.signup_endeavours (phone_number, operator_circle, android_id, device_id, instance_id, otp_matched,otp) " +
            " VALUES ( $1,$2,$3,$4,$5,false,$6)  RETURNING id;";
        var insertParam = [keyInfo.phoneNumber, keyInfo.operatorCircle, keyInfo.androidId, keyInfo.deviceId, keyInfo.instanceId, keyInfo.otp];

        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(insertQuery, insertParam).then(function (data) {
                    resolve(appService.returnSuccess(data));
                })
                .catch(function (err) {
                    console.log("error occured in user signedup", err);
                    appService.returnFailed().message = err.message;
                    let _logId = logSvc.logToFile("signupEndeavoursDb->createRegAttempt", false, err.message);
                    reject(appService.returnFailed(_logId));
                });
        });
    }
}

module.exports = new signupEndeavoursDb();