var pgHelper = require("./pgDbHelper");
var appService = require("./../../common/commonService/appService");
var logSvc = require("./../../common/commonService/loggerService");

class circleDb{

    isCircleOpened(circleName) {

        let _circleName = circleName.toUpperCase();
        var selectQuery = "select t.split_circle from (select regexp_split_to_table(circle_name, E'\\\\s+') AS split_circle FROM circlez) t where upper(t.split_circle) in (select Upper(regexp_split_to_table( $1 , E'\\\\s+')))";
        
        var selectParam = [_circleName];

        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(selectQuery, selectParam).then(function (data) {
                    if (data.length && data.length >= 1) {
                        resolve(appService.returnSuccess("",data[0]));
                    } else {
                        let logId = logSvc.logToFile("ruleService->isCircleOpend", false, data);
                        resolve(appService.returnFailed("",logId));
                    }
                })
                .catch(function (err) {
                    console.log("error occured in isCircleOpend", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }
}

module.exports = new circleDb();