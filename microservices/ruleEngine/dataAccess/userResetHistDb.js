var pgHelper = require("./pgDbHelper");
var appService = require("./../../common/commonService/appService");
var logSvc = require("./../../common/commonService/loggerService");

class userResetHistDb{

    userSignUpCountForXDays(phoneNumber,xDays) {

        var selectQuery = "SELECT count(1) FROM public.user_reset_hist where user_id = (select id from userz where phone_number = $1) and reset_type = 'Sign Up' and created_at > now() - interval '" + xDays + " Days';";
        
        var selectParam = [phoneNumber];
        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(selectQuery, selectParam).then(function (data) {
                    if (data.length && data.length >= 1) {
                        resolve(appService.returnSuccess("",data[0]));
                    } else {
                        let logId = logSvc.logToFile("ruleService->userSignUpCountForXDays", false, data);
                        resolve(appService.returnFailed("", logId));
                    }
                })
                .catch(function (err) {
                    console.log("error occured in userSignUpCountForXDays", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }
}

module.exports = new userResetHistDb();