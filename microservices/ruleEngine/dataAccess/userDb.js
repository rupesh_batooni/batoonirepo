//region [Imports]
var pgHelper = require("./pgDbHelper");
var appService = require("./../../common/commonService/appService");
//var logSvc = require("./../service/commonService/loggerService");
//endregion

class userDb {

    userCount() {
        //TODO: query count by role id as 3 for normal users
        var selectQuery = "select count(1) from public.userz;";

        var selectParam = [];
        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(selectQuery, selectParam).then(function (data) {
                    if (data.length == 0) {
                        resolve(appService.returnFailed());
                    } else {
                        resolve(appService.returnSuccess("", data[0]));
                    }
                })
                .catch(function (err) {
                    console.log("error occured in find regitstration attampt", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }
    findUser(phoneNumber) {
        //TODO: all query should be read from json file
        var selectQuery = "select * from public.userz where phone_number = $1;";

        var selectParam = [phoneNumber];
        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(selectQuery, selectParam).then(function (data) {
                    if (data.length == 0) {
                        resolve(appService.returnFailed());
                    } else {
                        resolve(appService.returnSuccess("", data[0]));
                    }
                })
                .catch(function (err) {
                    console.log("error occured in find regitstration attampt", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }

    createUser(user) {

        var insertQuery = "INSERT INTO public.userz (fname, lname,phone_number, email, gender,  pin_code,dob,city_id, circle_id, mobile_operator_id, app_version) " +
            " VALUES ( $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)  RETURNING id;";
        var insertParam = [user.firstName, user.lastName, user.reqInfo.phoneNumber, user.email, user.gender, user.pinCode, user.dob, user.cityId, user.circleId, user.mobileOperatorId, user.appVersion];

        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(insertQuery, insertParam).then(function (data) {
                    if (data.length == 0) {
                        resolve(appService.returnFailed());
                    } else {
                        resolve(appService.returnSuccess("", data));
                    }
                })
                .catch(function (err) {
                    console.log("error occured in user signedup", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }

    updateUser(restoreUser) {
        let user = restoreUser;
        var query = "UPDATE public.userz SET fname = $1, lname = $2, email = $3, gender = $4,  pin_code = $5,dob = $6,city_id = $7, circle_id = $8, mobile_operator_id = $9, app_version = $10, updated_at = now() where phone_number = $11 RETURNING id;";

        var param = [user.firstName, user.lastName, user.email, user.gender, user.pinCode, user.dob, user.cityId, user.circleId, user.mobileOperatorId, user.appVersion, user.phoneNumber];

        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(query, param).then(function (data) {
                    console.log("user update:", data);
                    if (data.length == 0) {
                        resolve(appService.returnFailed("User not been updated", data));
                    } else {
                        resolve(appService.returnSuccess("", data));
                    }

                })
                .catch(function (err) {
                    console.log("error occured in user signedup", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }

    updateToken(userDetail) {

        var query = "UPDATE public.userz SET user_token = $1 where phone_number = $2 RETURNING id;";

        var param = [userDetail.userToken, userDetail.phoneNumber];

        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(query, param).then(function (data) {
                    if (data.length == 0) {
                        resolve(appService.returnFailed("User not been updated", data));
                    } else {
                        resolve(appService.returnSuccess("", data));
                    }
                })
                .catch(function (err) {
                    console.log("error occured in user signedup", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }

    getUserToken(phoneNumber) {

        //TODO: all query should be read from json file
        var selectQuery = "select user_token from public.userz where phone_number = $1;";

        var selectParam = [phoneNumber];
        return new Promise(function (resolve, reject) {
            pgHelper.pgCommand(selectQuery, selectParam).then(function (data) {
                    if (data.length == 0) {
                        resolve(appService.returnFailed());
                    } else {
                        resolve(appService.returnSuccess("", data[0]));
                    }
                })
                .catch(function (err) {
                    console.log("error occured in find regitstration attampt", err);
                    reject(appService.returnFailed(err.message));
                });
        });
    }
}

module.exports = new userDb();