//#region [Imports] 
const request = require("request");
var appService = require("../../common/commonService/appService");
var logSvc = require("../../common/commonService/loggerService");
var signupEndvDb = require("../dataAccess/signupEndeavoursDb");
var userDb = require("../dataAccess/userDb");
var userResetHistDb = require("../dataAccess/userResetHistDb");
var utilitySvc = require("../../common/commonService/utilityService");
//#endregion

//#region [Variables] 
var authToken;
var circleCheckProviderUrl;
var smsProviderAuthKey;
//#endregion

class ruleService {

    //#region [Private] 

    constructor() {
        switch (appService.envPayToAll) {
            case "test":
                authToken = appService.payToall.test.access_token;
                circleCheckProviderUrl = appService.payToall.test.pay2all_number_check;
                smsProviderAuthKey = appService.secret.test.authkey;
                break;
            case "dev":
                authToken = appService.payToall.dev.access_token;
                circleCheckProviderUrl = appService.payToall.development.pay2all_number_check;
                smsProviderAuthKey = appService.secret.development.authkey;
                break;
            case "staging":
                authToken = appService.payToall.staging.access_token;
                circleCheckProviderUrl = appService.payToall.staging.pay2all_number_check;
                smsProviderAuthKey = appService.secret.staging.authkey;
                break;
            case "production":
                authToken = appService.payToall.production.access_token;
                circleCheckProviderUrl = appService.payToall.production.pay2all_number_check;
                smsProviderAuthKey = appService.secret.production.authkey;
                break;
            default:
                authToken = appService.payToall.test.access_token;
                circleCheckProviderUrl = appService.payToall.test.pay2all_number_check;
                smsProviderAuthKey = appService.secret.test.authkey;
                break;
        }
    }

    //#endregion

    getOperatorDetail(phoneNumber) {
        var options = {
            "method": "POST",
            url: circleCheckProviderUrl,
            "headers": {
                "content-type": "multipart/form-data",
                "accept": "application/json",
                "authorization": "Bearer " + authToken,
                "cache-control": "no-cache"
            },
            formData: {
                number: phoneNumber
            }
        };

        return new Promise(function (resolve, reject) {
            request(options, function (error, response, body) {
                if (error) reject(new Error(error));
                resolve(JSON.parse(body));
            });
        });
    }

    getBatooniCircleId(operatorCircle) { // return circle id from batooni_circle table
        // match and get circle id from batooni circle table

        return appService.returnSuccess();
    }

    //returnFailed if user does not exist or exist but dob has empty; return success when dob has value
    isUserDobExist() {

        return appService.returnSuccess();

    }


    // return max updatedDate matched with phone number
    async foundMaxNoOfSignUP(phoneNumber) {
        let _noOfSignUP = await userResetHistDb.userSignUpCountForXDays(phoneNumber, appService.maxSignupDaysLimit);
        if (_noOfSignUP.status && Number(appService.maxSignup) < _noOfSignUP.data.count) {
            return appService.returnSuccess(utilitySvc.Stringformat(appService.appMsg.errors.sign_up_count_exceed, phoneNumber, appService.maxSignupDaysLimit));
        }
        return appService.returnFailed();
    }

    //return true if user reset detail has last updated value older than xMonths
    isUserInactiveLastXMonths(xMonth) {

        return appService.returnSuccess();
    }

    createLedger(ledger) {

        return appService.returnSuccess();
    }

    generateOtp() {
        return Math.floor(100000 + Math.random() * 900000);
    }

    otpSendRequestToProvider(otp, phoneNumber) {

        if (appService.otpDefault.phoneNumber.length > 0) phoneNumber = appService.otpDefault.phoneNumber;

        var options = {
            method: "POST",
            url: appService.smsApi.link,
            headers: {
                "cache-control": "no-cache",
                "content-type": "application/x-www-form-urlencoded"
            },
            form: {
                authkey: smsProviderAuthKey,
                mobiles: phoneNumber,
                message: "The OTP for Batooni is " + otp,
                sender: "BATONI",
                route: "4"
            }
        };
        var _otp = otp;
        try {
            return new Promise(function (resolve, reject) {
                //TODO:
                //update code here
                if (!appService.otpDefault.sendToProvider) {
                    resolve({
                        type: "success",
                        generatedOtp: _otp
                    });
                }
                request(options, function (error, response, body) {
                    if (error) reject(error);
                    resolve({
                        type: "success"
                    });
                });
            });
        } catch (error) {
            let _logId = logSvc.logToFile("ruleService->otpSendRequestToProvider", false, appService.returnFailed().message);
            return appService.returnFailed("Error on sending otp request to provider " + appService.smsApi.link + ", Phone numeber: " + phoneNumber, _logId);
        }
    }

    async isOtpMatched(phoneNumber) {

        let result = await signupEndvDb.findLatestOtpMatch(phoneNumber);
        return result;
    }

    // Bonus On/Off: If Bonus setting is ON then the new user is credited with the sign-on Bonus.
    // check referral bonus on/off
    //if new user: credit sign up bonus in ledger
    //if existing user: check for Inactive for last 6 month; if inactive: credit sign up bonus in ledger
    // Determine bonus eligibility for new user 
    // Determine bonus eligibility for Inactive user for last 6 month
    // Bonus credit 
    //first_recharge: first_recharge is true for users who have got a bonus credit in their wallet - this will prevent further bonus credits.
    // Sign Up as New - Invalidate old ledger Data, 
    // Referral Bonus On/Off: If Referral Bonus is on and a new user (B) has used a referral code of an existing user (A) then A will get the referral bonus. // through job run

    async creditBonus(signUpUser, isNewUser) {

    }

    async foundMaxUserLimit() {
        let regCountResult = await userDb.userCount();
        if (Number(regCountResult.data.count) >= appService.maxUserRegistrationLimit) {
            return appService.returnSuccess();
        }
        return appService.returnFailed();
    }

    // async foundMaxUserLimit(){
    //     let regCountResult = await userDb.userCount();
    //         if (regCountResult.data.count >= appService.maxUserRegistrationLimit) {
    //             return appService.returnSuccess();
    //         }
    // }

}

module.exports = new ruleService();